from django.db import models, transaction
from .g_twilio import *
import twilio.base.exceptions

# Create your models here.
class StringLengths:
    DescLength = 15
    FullTitleLength = 50
    PhoneLength = 10
    ClientIdLength = 5
    MatterNumLength = 5
    FaxIdLength = 8


class Client(models.Model):
    ShortName = models.CharField(max_length=StringLengths.DescLength)
    FullName = models.CharField(max_length=StringLengths.FullTitleLength)
    ClientId = models.CharField(max_length=StringLengths.ClientIdLength, primary_key=True)

    @property
    def PrettyName(self):
        return str(self.ClientId).zfill(StringLengths.ClientIdLength) + ": " + str(self.ShortName)

    def __str__(self):
        return self.PrettyName


class Matter(models.Model):
    ShortName = models.CharField(max_length=StringLengths.DescLength)
    FullName = models.CharField(max_length=StringLengths.FullTitleLength)
    MatterNumber = models.IntegerField()
    ParentClient = models.ForeignKey(Client, on_delete=models.CASCADE)

    @property
    def MatterName(self):
        return str(self.MatterNumber).zfill(StringLengths.ClientIdLength) + ": " + str(self.ShortName)

    @property
    def PrettyName(self):
        result = str(self.ParentClient.ClientId).zfill(StringLengths.ClientIdLength)
        result += "."
        result +=  str(self.MatterNumber).zfill(StringLengths.MatterNumLength)
        return result

    def __str__(self):
        return self.PrettyName

class Fax(models.Model):
    # project specific fields
    ParentMatter = models.ForeignKey(Matter, on_delete=models.CASCADE)
    DateCreated = models.DateField(auto_now_add=True)

    # essential fields for faxes
    DestNumber = models.CharField(max_length=StringLengths.PhoneLength)
    Pdf = models.FileField(upload_to='pdfs/', null=True)

    # fields for data from twilio 
    FaxStatus = models.CharField(max_length=10, default='ready')
    TwilioId = models.CharField(max_length=50, null=True)
    TwilioErrorMessage = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "faxes"
        ordering = ['-pk']

    @property
    def FullId(self):
        return "FID" + str(self.id).zfill(StringLengths.FaxIdLength)

    def __str__(self):
        return self.FullId

    @classmethod
    def Create(cls, p_matter, p_dest_number, p_pdf):
        fax = cls(ParentMatter=p_matter, dest_number=p_dest_number, pdf=p_pdf)
        fax.save()
        return fax


    def Send(self):
        try:
            tid = TwilioSendText(self)
            print('new tid:' + str(tid))
            self.TwilioId = tid
        except twilio.base.exceptions.TwilioRestException as e:
            self.FaxStatus = "error"
            self.TwilioId = ""
            self.TwilioErrorMessage = e.msg
        self.save()

        return self.TwilioId

    def ParseStatus(self, p_status):
        with transaction.atomic():
            query = Fax.objects.select_for_update().get(pk=self.pk)
            print('updating status value')
            query.FaxStatus = p_status
            query.save()
        return self

    @property
    def IsSent(self):
        return self.FaxStatus == 'delivered'





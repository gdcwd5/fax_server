from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.core import serializers
from .models import Fax, Matter, Client
from .fax_api import *
from .forms import NewFaxForm, FilterFaxList
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


def ajax_matters(request, clientid):
    matters = ListMatters(clientid)
    matter_list = serializers.serialize('json', matters)
    return HttpResponse(matter_list, content_type="text/json")


def index(request):
    return redirect('list')


def new(request):
    client_list = ListClients()
    if request.method == 'POST':
        print("attempting to create new fax")
        form = NewFaxForm(request.POST, request.FILES)

        if form.is_valid():
            print("form valid")
            matter_number = form.cleaned_data['MatterNumber']
            print(matter_number)
            matter = Matter.objects.get(pk=matter_number)
            newfax = Fax(ParentMatter=matter, DestNumber=form.cleaned_data['SendNumber'])
            if request.FILES:
                newfax.Pdf = request.FILES['UploadPDF']
            newfax.save()
            newfax.Send()
            return redirect('detail', fid=newfax.id)
        else:
            print("form was not valid")
    else:
        print("opening blank form")
        form = NewFaxForm()

    context = {'form': form, 'clients': client_list}
    return render(request, 'new.html', context)


def detail(request, fid):
    fax = {}

    if GetFax(fid):
        fax = Fax.objects.get(pk=fid)
        matter = Matter.objects.get(pk=fax.ParentMatter.pk)
        client = Client.objects.get(pk=matter.ParentClient.pk)
    else:
        # error
        return list(request)

    context = {'fax': fax, 'matter': matter, 'client': client}
    return render(request, 'detail.html', context)


@csrf_exempt
def list(request):
    p = request.GET
    faxes = Fax.objects.all()
    client_list = ListClients()

    if request.method == 'POST':
        form = FilterFaxList(request.POST)
        if form.is_valid():
            print('form valid')
            client_number = form.cleaned_data['ClientNumber']
            matter_number = form.cleaned_data['MatterNumber']
            if client_number and matter_number:
                if Matter.objects.filter(ParentClient=client_number, MatterNumber=matter_number):
                    matter = Matter.objects.get(ParentClient=client_number, MatterNumber=matter_number)
                    faxes = Fax.objects.filter(ParentMatter=matter)
                else:
                    faxes = []
                pass
            elif client_number and Client.objects.filter(pk=client_number):
                parent_client = Client.objects.get(pk=client_number)
                matters = parent_client.matter_set.all()
                faxes = []
                for m in matters:
                    for f in Fax.objects.filter(ParentMatter=m):
                        faxes.append(f)
                pass
            else:
                faxes = []
    else:
        form = FilterFaxList()

    context = {'faxes': faxes, 'clients': client_list, 'form': form}
    return render(request, 'list.html', context)


def api_fax_media(request, fid):
    f = Fax.objects.get(id=fid)

    return HttpResponseRedirect(f.Pdf.url)


@csrf_exempt
def api_fax_callback(request):
    print('a request')

    if request.method == 'POST':
        r = request.POST
        print('make that a post request')
        fid = request.POST.get('SmsSid')
        print('found:' + str(fid))
        fax_list = Fax.objects.filter(TwilioId=fid)
        for fax in fax_list:
            print('new status:' + request.POST.get('SmsStatus'))
            fax.ParseStatus(request.POST.get('SmsStatus'))

    return JsonResponse({})


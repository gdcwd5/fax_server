from django import forms
from .models import StringLengths
from django.core import validators

PhoneRegex = "^[0-9]{" + str(StringLengths.PhoneLength) + "}$"
ClientNumberRegex = "^[0-9]{1," + str(StringLengths.ClientIdLength) + "}$"
MatterNumberRegex = "^[0-9]{1," + str(StringLengths.MatterNumLength) + "}$"


# use consts for phone and number lengths?
class NewFaxForm(forms.Form):
    SendNumber = forms.RegexField(regex=PhoneRegex, label='Destination Number')
    ClientNumber = forms.RegexField(regex=ClientNumberRegex, label='Client Number')
    MatterNumber = forms.RegexField(regex=MatterNumberRegex, label='Matter Number')
    UploadPDF = forms.FileField(label='Upload PDF', validators=[validators.FileExtensionValidator(['pdf'])])

class FilterFaxList(forms.Form):
    ClientNumber = forms.RegexField(regex=ClientNumberRegex, label='Client Number')
    MatterNumber = forms.RegexField(regex=MatterNumberRegex, label='Matter Number', required=False)


from FaxServer.models import Client, Matter, Fax

# list all clients
def ListClients():
    return Client.objects.all()


def GetFax(p_id):
    return Fax.objects.filter(id=p_id)


# list all matters for a specific client
def ListMatters(client_id):
    return Matter.objects.filter(ParentClient=client_id)


# convert a client-matter composite reference to a unique matter reference
def LookupMatter(client_id, matter):
    return Matter.objects.filter(parent_client=client_id, matter_number=matter)


# list all faxes
def ListFaxes():
    return Fax.objects.all()


# list all faxes for a unique matter reference
def GetMatterFaxes(matter):
    return Fax.objects.filter(matter_id=matter)


# list all faxes for a composite matter reference
def GetMatterFaxes(client_id, matter):
    return GetMatterFaxes(LookupMatter(client_id, matter))


# list all faxes for a client
def GetClientFaxes(client_id):
    matters = ListMatters(client_id)
    faxes = []
    for matter_ref in matters:
        faxes.append(GetMatterFaxes(matter_ref))
    return faxes



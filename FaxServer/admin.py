from django.contrib import admin
from .models import Fax, Matter, Client
# Register your models here.

admin.site.register(Fax)
admin.site.register(Matter)
admin.site.register(Client)

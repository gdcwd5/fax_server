from twilio.rest import Client
import twilio.base.exceptions

# not globals?
AccountSid = 'ACd09bb53515a2e58d20c27b53a9424d13'
AuthToken = 'e6b234745b5d7cb90df72bea593e6e58'
TwilioClient = Client(AccountSid, AuthToken)
BaseUrl = 'http://fax.cline.group'
SendingPhoneNumber = '+12565872658'


def TwilioSend(p_fax):
    return TwilioSendText(p_fax)


def TwilioSendFax(p_fax):
    sent_fax = TwilioClient.fax.faxes \
        .create(
            from_=SendingPhoneNumber,
            to='+1' + str(p_fax.DestNumber),
            media_url=BaseUrl + str(p_fax.Pdf.url)
        )

    return sent_fax.sid


def TwilioSendText(p_fax):
    try:
        sent_text = TwilioClient.messages.create(
                from_=SendingPhoneNumber,
                to='+1' + str(p_fax.DestNumber),
                body=BaseUrl + str(p_fax.Pdf.url),
                status_callback='http://fax.cline.group/fax-server/api/fax-status-callback/'
            )
        sid = sent_text.sid
    except twilio.base.exceptions.TwilioRestException:
        raise

    return sid

from django.apps import AppConfig


class FaxServerConfig(AppConfig):
    name = 'FaxServer'

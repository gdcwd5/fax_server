from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.list, name='list'),
    path('new/', views.new, name='new'),
    path('detail/<int:fid>/', views.detail, name='detail'),
    path('ajax/matters/<int:clientid>/', views.ajax_matters, name='ajax_matters'),
    path('download/<int:fid>/', views.api_fax_media, name='api_fax_media'),
    path('api/fax-status-callback/', views.api_fax_callback, name='api_fax_callback')
]